package com.unju.tp6balero;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;


@Controller

public class ControlProducto {
    List<Producto> listaP = new ArrayList<Producto>();
    List<Pago> listaPago = new ArrayList<Pago>();
    List<Pedido> listaPedido = new ArrayList<Pedido>();

    {
        listaP.add(new Producto("Combo 1", "1 hamburguesa + papa frita", 1400));
        listaP.add(new Producto("Combo 2", "1 hamburguesa + gaseosa + papa frita", 1800));
        listaP.add(new Producto("Combo 3", "1 hamburguesa con queso + gaseosa + papa frita", 2000));
        listaP.add(new Producto("Combo 4", "1 hamburguesa con queso y jamon + gaseosa grande + doble porcion de papas frita", 2200));
    };

    {
        listaPago.add(new Pago("Contado", "-", 0));
        listaPago.add(new Pago("Credito", "$400 de recargo sobre el total", 400));
        listaPago.add(new Pago("Debito", "5% de descuento sobre el totol", 5));
    };

    @GetMapping("/formulario")
    public String formulario(Model model){
        Pedido pedido = new Pedido();
        model.addAttribute("productos", listaP);
        model.addAttribute("pagos", listaPago);
        model.addAttribute("pedido", pedido);
        return "formulario";
    }

    @PostMapping("/guardar")
    public String guardarCompra(@ModelAttribute Pedido pedido){
        listaPedido.add(pedido);
        return "resultado";
    }
    
}
