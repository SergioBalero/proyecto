package com.unju.tp6balero;

public class Pago {
    private String forma;
    private String detalle;
    private int descuento;
    
    public Pago() {
    }

    public Pago(String forma, String detalle, int descuento) {
        this.forma = forma;
        this.detalle = detalle;
        this.descuento = descuento;
    }

    public String getForma() {
        return forma;
    }

    public void setForma(String forma) {
        this.forma = forma;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    
}
