package com.unju.tp6balero;

public class Pedido {
    private String combo;
    private double precio;
    private int cantidad;
    private String pago;

    public Pedido() {
    }

    public Pedido(String combo, double precio, int cantidad, String pago) {
        this.combo = combo;
        this.precio = precio;
        this.cantidad = cantidad;
        this.pago = pago;
    }

    public String getCombo() {
        return combo;
    }
    public void setCombo(String combo) {
        this.combo = combo;
    }
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public int getCantidad() {
        return cantidad;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
    public String getPago() {
        return pago;
    }
    public void setPago(String pago) {
        this.pago = pago;
    }
    
    

}
