package com.unju.tp6balero;

public class Producto {
    private String combo;
    private String detalle;
    private double precio;

    public Producto() {
    }

    public Producto(String combo, String detalle, double precio) {
        this.combo = combo;
        this.detalle = detalle;
        this.precio = precio;
    }

    public String getCombo() {
        return combo;
    }

    public void setCombo(String combo) {
        this.combo = combo;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    
}
