package com.unju.tp6balero;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp6baleroApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp6baleroApplication.class, args);
	}

}
